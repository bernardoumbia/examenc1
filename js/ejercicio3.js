const form = document.getElementById("data-form");

      form.addEventListener("submit", function (e) {
         e.preventDefault();

         const nombre = document.getElementById("nombre").value;
         const edad = document.getElementById("edad").value;
         const sexo = document.querySelector('input[name="sexo"]:checked').value;
         const comentarios = document.getElementById("comentarios").value;

         // Crear el mensaje de correo electrónico
         const emailSubject = "Formulario de Datos";
         const emailBody = `Nombre: ${nombre}\nEdad: ${edad}\nSexo: ${sexo}\nComentarios: ${comentarios}`;

         // Abrir el cliente de correo predeterminado del usuario
         const emailLink = `mailto:microinformaticamx@gmail.com?subject=${encodeURIComponent(emailSubject)}&body=${encodeURIComponent(emailBody)}`;
         window.location.href = emailLink;
      });