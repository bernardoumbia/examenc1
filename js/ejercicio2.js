const total = document.querySelector('#total');
const noAprobados = document.querySelector('#noAprobados');
const aprobados = document.querySelector('#aprobados');
const noAprobadosPromedio = document.querySelector('#noAprobadosPromedio');
const aprobadosPromedio = document.querySelector('#aprobadosPromedio');
const promedioGral = document.querySelector('#promedioGral');


const array = [];

for (let x = 0; x < 40; x++) {
   array.push(Math.floor(Math.random() * 11)); // Números aleatorios entre 0 y 10
}

const generar = document.querySelector("#generar");

generar.addEventListener('click', (e) => {
   e.preventDefault();

   const totalValue = array.length;
   const noAprobadosValue = array.filter(value => value < 6).length;
   const aprobadosValue = totalValue - noAprobadosValue;
   const noAprobadosPromedioValue = array.reduce((acc, value) => value < 6 ? acc + value : acc, 0) / noAprobadosValue || 0;
   const aprobadosPromedioValue = array.reduce((acc, value) => value >= 6 ? acc + value : acc, 0) / aprobadosValue || 0;
   const promedioGralValue = array.reduce((acc, value) => acc + value, 0) / array.length || 0;

   total.textContent = `Total: [${array.join(', ')}]`;
   noAprobados.textContent = `No Aprobados: ${noAprobadosValue}`;
   aprobados.textContent = `Aprobados: ${aprobadosValue}`;
   noAprobadosPromedio.textContent = `Promedio de No Aprobados: ${noAprobadosPromedioValue.toFixed(2)}`;
   aprobadosPromedio.textContent = `Promedio de Aprobados: ${aprobadosPromedioValue.toFixed(2)}`;
   promedioGral.textContent = `Promedio General: ${promedioGralValue.toFixed(2)}`;
});
